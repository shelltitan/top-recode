//=============================================================================
// FileName: JobType.h
// Creater: ZhangXuedong
// Date: 2005.03.18
// Comment: Job Type
//=============================================================================

#ifndef JOBTYPE_H
#define JOBTYPE_H

constexpr long JOB_TYPE_XINSHOU = 0;	   // "新手"
constexpr long JOB_TYPE_JIANSHI = 1;	   // "剑士"
constexpr long JOB_TYPE_LIEREN = 2;		   // "猎人"
constexpr long JOB_TYPE_SHUISHOU = 3;	  // "水手"
constexpr long JOB_TYPE_MAOXIANZHE = 4;	// "冒险者"
constexpr long JOB_TYPE_QIYUANSHI = 5;	 // "祈愿使"
constexpr long JOB_TYPE_JISHI = 6;		   // "技师"
constexpr long JOB_TYPE_SHANGREN = 7;	  // "商人"
constexpr long JOB_TYPE_JUJS = 8;		   // "巨剑士"
constexpr long JOB_TYPE_SHUANGJS = 9;	  // "双剑士"
constexpr long JOB_TYPE_JIANDUNSHI = 10;   // "剑盾士"
constexpr long JOB_TYPE_XUNSHOUSHI = 11;   // "驯兽师"
constexpr long JOB_TYPE_JUJISHOU = 12;	 // "狙击手"
constexpr long JOB_TYPE_SHENGZHIZHE = 13;  // "圣职者"
constexpr long JOB_TYPE_FENGYINSHI = 14;   // "封印师"
constexpr long JOB_TYPE_CHUANZHANG = 15;   // "船长"
constexpr long JOB_TYPE_HANGHAISHI = 16;   // "航海士"
constexpr long JOB_TYPE_BAOFAHU = 17;	  // "暴发户"
constexpr long JOB_TYPE_GONGCHENGSHI = 18; // "工程师"

constexpr long MAX_JOB_TYPE = 19; // 最大职业类型

//extern const char	*g_szJobName[MAX_JOB_TYPE];

#endif // JOBTYPE_H